Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Todoman
Upstream-Contact: Hugo Osvaldo Barrera <hugo@barrera.io>
Source: https://github.com/pimutils/todoman

Files: *
Copyright:
  2015-2020  Hugo Osvaldo Barrera <hugo@barrera.io>
License-Grant:
 Todoman is licensed under the ISC licence.
License: ISC
Reference:
 LICENCE
 README.rst
 pyproject.toml

Files:
 todoman/widgets.py
Copyright:
  2013-2016  Christian Geier et al.
  2016-2020  Hugo Osvaldo Barrera
License: Expat

Files:
 debian/*
Copyright:
  2016  Félix Sipma <felix+debian@gueux.org>
  2023  Jonas Smedegaard <dr@jones.dk>
License-Grant:
 This package is free software;
 you can redistribute it and/or modify it
 under the terms of the GNU General Public License
 as published by the Free Software Foundation,
 either version 3 of the License,
 or (at your option) any later version.
License: GPL-3+
Reference:
 debian/copyright

License: ISC
 Permission to use, copy, modify, and/or distribute this software
 for any purpose with or without fee
 is hereby granted,
 provided that the above copyright notice and this permission notice
 appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS"
 AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE
 INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE
 FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES
 OR ANY DAMAGES WHATSOEVER
 RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 ARISING OUT OF OR IN CONNECTION WITH
 THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: Expat
 Permission is hereby granted, free of charge,
 to any person obtaining a copy
 of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights
 to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS",
 WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO
 THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
 OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: GPL-3+
Reference: /usr/share/common-licenses/GPL-3
